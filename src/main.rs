/*
Copyright 2019 Andrew Walbran

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

extern crate tokio;
extern crate websocket;

use std::env;
use std::fmt::Debug;
use std::net::SocketAddr;
use std::net::ToSocketAddrs;

use websocket::r#async::{Server, TcpStream};
use websocket::client::r#async::Client;
use websocket::message::OwnedMessage;
use websocket::result::WebSocketError;
use websocket::server::InvalidConnection;

use futures::{stream, Future, Sink, Stream};
use futures::sync::mpsc;
use tokio::runtime::TaskExecutor;
use tokio::codec::{Framed, LinesCodec};

struct Session {
  server_address: SocketAddr,
  client: Client<TcpStream>,
}

impl Session {
  pub fn new(server_address: &SocketAddr, client: Client<TcpStream>) -> Self {
    Session {
      server_address: *server_address,
      client: client,
    }
  }

  pub fn run(self) -> impl Future<Item = (), Error = WebSocketError> + Send {
    let (ws_sink, ws_stream) = self.client.split();
    let server = TcpStream::connect(&self.server_address);
    server.map_err(|e| dbg!(e).into())
        .and_then(|s| {
      let (server_sink, server_stream) = Framed::new(s, LinesCodec::new()).split();
      let (server_sink_tx, server_sink_rx) = mpsc::unbounded();

      let server_output = server_stream
        .map_err(|e| dbg!(e).into())
        .map(|mut line| {
          line.push('\n');
          OwnedMessage::Text(line)
        })
        .chain(stream::iter_ok(vec![OwnedMessage::Close(None)]));

      let ws: Box<Future<Item = (), Error = WebSocketError> + Send> = Box::new(ws_stream
        .take_while(|m| Ok(!m.is_close()))
        .filter_map(move |m| {
          match m {
            OwnedMessage::Ping(p) => Some(OwnedMessage::Pong(p)),
            OwnedMessage::Pong(_) => None,
            OwnedMessage::Text(t) => {
              let tx = server_sink_tx.clone();
              // Sending to an unbounded queue should never block or fail, so this is safe.
              tx.send(t).wait().unwrap();
              None
            },
            _ => Some(m),
          }
        })
        .select(server_output)
        .forward(ws_sink)
        .and_then(|(_, sink)| sink.send(OwnedMessage::Close(None)))
        .map_err(|e| dbg!(e).into())
        .map(|_| ()));

      let tcp: Box<Future<Item = (), Error = WebSocketError> + Send> = Box::new(server_sink_rx
        .map_err(|()| std::io::Error::from_raw_os_error(0))
        .forward(server_sink)
        .map_err(|e| dbg!(e).into())
        .map(|_| ()));

      tcp.join(ws).map(|((), ())| ())
    })
  }
}

fn run(listen_address: &impl ToSocketAddrs, server_address: &impl ToSocketAddrs) {
  let server_address = server_address.to_socket_addrs().unwrap().next().unwrap();
  let mut runtime = tokio::runtime::Builder::new().build().unwrap();
	let executor = runtime.executor();
	let server = Server::bind(listen_address, &tokio::reactor::Handle::default()).unwrap();

  let f = server.incoming()
    .then(|conn| -> Result<_, ()> {
      Ok(match conn {
        Ok(v) => Some(v),
        Err(InvalidConnection{ref error, ..}) => {
          println!("Connection error: {}", error);
          None
        },
      })
    })
    .filter_map(|x| x)
    .for_each(move |(upgrade, addr)| {
      println!("New client {}", addr);

      let f = upgrade.accept()
        .and_then(move |(s, _)| {
          let session = Session::new(&server_address, s);
          session.run()
        });

      spawn_future(f, "Client Status", &executor);
      Ok(())
    });

  runtime.block_on(f).unwrap();
}

fn main() {
  let args: Vec<String> = env::args().collect();

  if args.len() != 3 {
    println!("Usage:");
    println!("  $ {} <listen address>:<port> <backend server address>:<port>", &args[0]);
    return;
  }

  run(&args[1], &args[2])
}

fn spawn_future<F, I, E>(f: F, desc: &'static str, executor: &TaskExecutor)
where
	F: Future<Item = I, Error = E> + 'static + Send,
	E: Debug,
{
	executor.spawn(
		f.map_err(move |e| println!("{}: '{:?}'", desc, e))
			.map(move |_| println!("{}: Finished.", desc)),
	);
}
