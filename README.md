# wsmoo

wsmoo is a text-only WebSocket to TCP relay. It was designed to be used by the [Arthnor web interface](https://gitlab.com/arthnor/arthnorweb), but may be useful for others building web interfaces to text-based services.
